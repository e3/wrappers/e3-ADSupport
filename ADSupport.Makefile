#
#  Copyright (c) 2019      Jeong Han Lee
#  Copyright (c) 2019      European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
#
# Author  : Jeong Han Lee
# email   : jeonghan.lee@gmail.com
# Date    : Friday, September 13 15:16:36 CEST 2019
# version : 0.0.8
#

## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile

APP:=supportApp

OS_default:=os/default
OS_Linux:=os/Linux

USR_CFLAGS   += -Wno-unused-variable
USR_CFLAGS   += -Wno-unused-function
USR_CFLAGS   += -Wno-unused-but-set-variable
USR_CPPFLAGS += -Wno-unused-variable
USR_CPPFLAGS += -Wno-unused-function
USR_CPPFLAGS += -Wno-unused-but-set-variable

USR_INCLUDES += -I$(PREFIX)/include
# Ubuntu needs the following option
USR_LDFLAGS += -Wl,--no-as-needed



##########################################################################
#
# General
#
##########################################################################
#
# Module activation and internal/external variables are set in
# $(TOP)/configure/CONFIG_OPTIONS
#
# 2020-10-19: - All modules have been switched to build the version that 
#               ships with ADSupport. (WL)
#             - Bitshuffle has been disabled due to function name
#               collisions with BLOSC. Also disabled in ADCore. (WL)


##########################################################################
#
# GraphicsMagick, GraphicsMagick++
#
##########################################################################
#
# 2020-10-20: corei7-poky cross-compile toolchain is missing at least one 
#             header file (X11/extensions/shape.h) so build only for x86_64
#             architecture. This is implemented in
#             Makefiles/GraphicsMagick.Makefile (WL)

ifeq ($(WITH_GRAPHICSMAGICK),YES)
ifeq ($(GRAPHICSMAGICK_EXTERNAL),NO)

include $(where_am_I)/GraphicsMagick.Makefile

endif # ($(GRAPHICSMAGICK_EXTERNAL),NO)
endif # ($(WITH_GRAPHICSMAGICK),YES)


##########################################################################
#
# nexus
#
##########################################################################

ifeq ($(WITH_NEXUS),YES)
ifeq ($(NEXUS_EXTERNAL),NO)

  include $(where_am_I)/nexus.Makefile

endif # ($(NEXUS_EXTERNAL),NO)
endif # ($(WITH_NEXUS),YES)


##########################################################################
#
# hdf5
#
##########################################################################

ifeq ($(WITH_HDF5),YES)
ifeq ($(HDF5_EXTERNAL),NO)

  include $(where_am_I)/hdf5.Makefile

else # ($(HDF5_EXTERNAL),NO)
# We have to combine libADSupport with libhdf5, so
# we need to add them here if you do use EXTERNAL HDF5
# We don't use the MPICH2 and OpenMPI, but use serial version in Debian
#
# CentOS    : /usr/local/lib
# ESS Yocto : $(SDKTARGETSYSROOT)/usr/lib64
# Debian    : /usr/lib/x86_64-linux-gnu/hdf5/serial
#

  LIB_SYS_LIBS += hdf5
  LIB_SYS_LIBS += hdf5_hl

endif # ($(HDF5_EXTERNAL),NO)
endif # ($(WITH_HDF5),YES)





ifeq ($(WITH_BITSHUFFLE),YES)
ifeq ($(BITSHUFFLE_EXTERNAL),NO)

  include $(where_am_I)/bitshuffle.Makefile

endif # ($(BITSHUFFLE_EXTERNAL),NO)
endif # ($(WITH_BITSHUFFLE),YES)



ifeq ($(WITH_SZIP),YES)
ifeq ($(SZIP_EXTERNAL),NO)

  include $(where_am_I)/szip.Makefile

endif # ($(SZIP_EXTERNAL),NO)
endif # ($(WITH_SZIP),YES)



##########################################################################
#
# xml2
#
##########################################################################

###
###
### We cannot use XML2 within ADSupport, because of
### config.h/config_32.h/config_64.h
### However, we install libxml2 in CentOS, Yocto Linux
### as the external library
###
### This space will leave here for the future, when
### ....
###
ifeq ($(WITH_XML2),YES)
ifeq ($(XML2_EXTERNAL),NO)

  include $(where_am_I)/xml2.Makefile

else
  LIB_SYS_LIBS += xml2
  USR_INCLUDES += -I$(PREFIX)/include/libxml2
endif # ($(XML2_EXTERNAL),NO)
endif # ($(WITH_XML2),YES)


##########################################################################
#
# tiff
#
##########################################################################

ifeq ($(WITH_TIFF0),YES)
ifeq ($(TIFF_EXTERNAL),NO)

  include $(where_am_I)/tiff.Makefile

else
  LIB_SYS_LIBS += tiff
endif # ($(TIFF_EXTERNAL),NO)
endif # ($(WITH_TIFF0),YES)




ifeq ($(WITH_BLOSC),YES)
ifeq ($(BLOSC_EXTERNAL),NO)

  include $(where_am_I)/blosc.Makefile

else
LIB_SYS_LIBS += blosc
endif # ($(BLOSC_EXTERNAL),NO)
endif # ($(WITH_BLOSC),YES)


##########################################################################
#
# zlib
#
##########################################################################

ifeq ($(WITH_ZLIB),YES)
# We will use ADSupport ZLIB
ifeq ($(ZLIB_EXTERNAL),NO)

  include $(where_am_I)/zlib.Makefile

endif # ($(ZLIB_EXTERNAL),NO)
endif # ($(WITH_ZLIB),YES)


##########################################################################
#
# jpeg
#
##########################################################################

ifeq ($(WITH_JPEG),YES)
ifeq ($(JPEG_EXTERNAL),NO)

  include $(where_am_I)/jpeg.Makefile

else
  LIB_SYS_LIBS += jpeg
endif # ($(JPEG_EXTERNAL),NO)
endif # ($(WITH_JPEG),YES)



##########################################################################
#
# netCDF
#
##########################################################################

ifeq ($(WITH_NETCDF),YES)
ifeq ($(NETCDF_EXTERNAL),NO)

  include $(where_am_I)/netCDF.Makefile

else
  LIB_SYS_LIBS += netcdf
endif # ($(NETCDF_EXTERNAL),NO)

endif # ($(WITH_NETCDF),YES)


##########################################################################
#
# cbf
#
##########################################################################

include $(where_am_I)/cbf.Makefile


vlibs:

.PHONY: vlibs
