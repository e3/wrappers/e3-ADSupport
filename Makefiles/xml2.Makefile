XML2TOP = $(APP)/xml2Src

# Need _REENTRANT flag on Linux for threads to build correctly
USR_CFLAGS_Linux += -D_REENTRANT

USR_INCLUDES += -I$(where_am_I)$(XML2TOP)
USR_INCLUDES += -I$(where_am_I)$(XML2TOP)/$(OS_Linux)
USR_INCLUDES += -I$(where_am_I)$(XML2TOP)/$(OS_default)/libxml

HEADERS += $(XML2TOP)/$(OS_Linux)/config_32.h
HEADERS += $(XML2TOP)/$(OS_Linux)/config_64.h

XML_INCS += DOCBparser.h
XML_INCS += globals.h
XML_INCS += tree.h
XML_INCS += xmlregexp.h
XML_INCS += HTMLparser.h
XML_INCS += HTMLtree.h
XML_INCS += SAX.h
XML_INCS += SAX2.h
XML_INCS += c14n.h
XML_INCS += catalog.h
XML_INCS += chvalid.h
XML_INCS += debugXML.h
XML_INCS += dict.h
XML_INCS += encoding.h
XML_INCS += entities.h
XML_INCS += hash.h
XML_INCS += list.h
XML_INCS += nanoftp.h
XML_INCS += nanohttp.h
XML_INCS += parser.h
XML_INCS += parserInternals.h
XML_INCS += pattern.h
XML_INCS += relaxng.h
XML_INCS += schemasInternals.h
XML_INCS += schematron.h
XML_INCS += threads.h
XML_INCS += uri.h
XML_INCS += valid.h
XML_INCS += xinclude.h
XML_INCS += xlink.h
XML_INCS += xmlIO.h
XML_INCS += xmlautomata.h
XML_INCS += xmlerror.h
XML_INCS += xmlexports.h
XML_INCS += xmlmemory.h
XML_INCS += xmlreader.h
XML_INCS += xmlsave.h
XML_INCS += xmlschemas.h
XML_INCS += xmlschemastypes.h
XML_INCS += xpointer.h
XML_INCS += xmlstring.h
XML_INCS += xmlunicode.h
XML_INCS += xmlversion.h
XML_INCS += xmlwriter.h
XML_INCS += xpath.h
XML_INCS += xpathInternals.h
XML_INCS += xmlmodule.h

HEADERS += $(addprefix $(XML2TOP)/$(OS_default)/libxml/, $(XML_INCS))
HEADERS += $(XML2TOP)/nanohttp_stream.h

SOURCES += $(XML2TOP)/buf.c
SOURCES += $(XML2TOP)/c14n.c
SOURCES += $(XML2TOP)/catalog.c
SOURCES += $(XML2TOP)/chvalid.c
SOURCES += $(XML2TOP)/debugXML.c
SOURCES += $(XML2TOP)/dict.c
SOURCES += $(XML2TOP)/DOCBparser.c
SOURCES += $(XML2TOP)/xml2src_encoding.c
SOURCES += $(XML2TOP)/entities.c
SOURCES += $(XML2TOP)/error.c
SOURCES += $(XML2TOP)/globals.c
SOURCES += $(XML2TOP)/hash.c
SOURCES += $(XML2TOP)/HTMLparser.c
SOURCES += $(XML2TOP)/HTMLtree.c
SOURCES += $(XML2TOP)/legacy.c
SOURCES += $(XML2TOP)/list.c
SOURCES += $(XML2TOP)/nanoftp.c
SOURCES += $(XML2TOP)/nanohttp.c
SOURCES += $(XML2TOP)/parser.c
SOURCES += $(XML2TOP)/parserInternals.c
SOURCES += $(XML2TOP)/pattern.c
SOURCES += $(XML2TOP)/relaxng.c
SOURCES += $(XML2TOP)/SAX2.c
SOURCES += $(XML2TOP)/SAX.c
SOURCES += $(XML2TOP)/schematron.c
SOURCES += $(XML2TOP)/threads.c
SOURCES += $(XML2TOP)/tree.c
SOURCES += $(XML2TOP)/uri.c
SOURCES += $(XML2TOP)/valid.c
SOURCES += $(XML2TOP)/xinclude.c
SOURCES += $(XML2TOP)/xlink.c
SOURCES += $(XML2TOP)/xmlIO.c
SOURCES += $(XML2TOP)/xmlmemory.c
SOURCES += $(XML2TOP)/xmlreader.c
SOURCES += $(XML2TOP)/xmlregexp.c
SOURCES += $(XML2TOP)/xmlmodule.c
SOURCES += $(XML2TOP)/xmlsave.c
SOURCES += $(XML2TOP)/xmlschemas.c
SOURCES += $(XML2TOP)/xmlschemastypes.c
SOURCES += $(XML2TOP)/xmlunicode.c
SOURCES += $(XML2TOP)/xmlwriter.c
# The following one includes "trionan.c"
SOURCES += $(XML2TOP)/xpath.c
SOURCES += $(XML2TOP)/xpointer.c
SOURCES += $(XML2TOP)/xmlstring.c

SOURCES += $(XML2TOP)/nanohttp_stream.c

KEEP_HEADER_SUBDIRS += $(XML2TOP)/$(OS_default)


