
JPEGTOP = $(APP)/jpegSrc

USR_INCLUDES += -I$(where_am_I)$(JPEGTOP)
USR_INCLUDES += -I$(where_am_I)$(JPEGTOP)/$(OS_Linux)
USR_INCLUDES += -I$(where_am_I)$(JPEGTOP)/$(OS_default)

HEADERS += $(JPEGTOP)/$(OS_default)/jpeglib.h
HEADERS += $(JPEGTOP)/$(OS_Linux)/jconfig.h
HEADERS += $(JPEGTOP)/$(OS_Linux)/jmorecfg.h
HEADERS += $(JPEGTOP)/$(OS_default)/jerror.h



SOURCES += $(JPEGTOP)/jaricom.c
SOURCES += $(JPEGTOP)/jcapimin.c
SOURCES += $(JPEGTOP)/jcapistd.c
SOURCES += $(JPEGTOP)/jcarith.c
SOURCES += $(JPEGTOP)/jccoefct.c
SOURCES += $(JPEGTOP)/jccolor.c
SOURCES += $(JPEGTOP)/jcdctmgr.c
SOURCES += $(JPEGTOP)/jchuff.c
SOURCES += $(JPEGTOP)/jcinit.c
SOURCES += $(JPEGTOP)/jcmainct.c
SOURCES += $(JPEGTOP)/jcmarker.c
SOURCES += $(JPEGTOP)/jcmaster.c
SOURCES += $(JPEGTOP)/jcomapi.c
SOURCES += $(JPEGTOP)/jcparam.c
SOURCES += $(JPEGTOP)/jcprepct.c
SOURCES += $(JPEGTOP)/jcsample.c
SOURCES += $(JPEGTOP)/jctrans.c
SOURCES += $(JPEGTOP)/jdapimin.c
SOURCES += $(JPEGTOP)/jdapistd.c
SOURCES += $(JPEGTOP)/jdarith.c
SOURCES += $(JPEGTOP)/jdatadst.c
SOURCES += $(JPEGTOP)/jdatasrc.c
SOURCES += $(JPEGTOP)/jdcoefct.c
SOURCES += $(JPEGTOP)/jdcolor.c
SOURCES += $(JPEGTOP)/jddctmgr.c
SOURCES += $(JPEGTOP)/jdhuff.c
SOURCES += $(JPEGTOP)/jdinput.c
SOURCES += $(JPEGTOP)/jdmainct.c
SOURCES += $(JPEGTOP)/jdmarker.c
SOURCES += $(JPEGTOP)/jdmaster.c
SOURCES += $(JPEGTOP)/jdmerge.c
SOURCES += $(JPEGTOP)/jdpostct.c
SOURCES += $(JPEGTOP)/jdsample.c
SOURCES += $(JPEGTOP)/jdtrans.c
SOURCES += $(JPEGTOP)/jerror.c
SOURCES += $(JPEGTOP)/jfdctflt.c
SOURCES += $(JPEGTOP)/jfdctfst.c
SOURCES += $(JPEGTOP)/jfdctint.c
SOURCES += $(JPEGTOP)/jidctflt.c
SOURCES += $(JPEGTOP)/jidctfst.c
SOURCES += $(JPEGTOP)/jidctint.c
SOURCES += $(JPEGTOP)/jquant1.c
SOURCES += $(JPEGTOP)/jquant2.c
SOURCES += $(JPEGTOP)/jutils.c
SOURCES += $(JPEGTOP)/jmemmgr.c
SOURCES += $(JPEGTOP)/jmemnobs.c

SOURCES += $(JPEGTOP)/decompressJPEG.c

