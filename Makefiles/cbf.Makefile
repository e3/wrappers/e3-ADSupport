CBFTOP = $(APP)/cbfSrc


HEADERS += $(CBFTOP)/cbf_ad.h
HEADERS += $(CBFTOP)/cbf_tree_ad.h
HEADERS += $(CBFTOP)/cbf_context_ad.h
HEADERS += $(CBFTOP)/cbf_file_ad.h
HEADERS += $(CBFTOP)/global_ad.h
HEADERS += $(CBFTOP)/md5_ad.h

SOURCES += $(CBFTOP)/cbf.c
SOURCES += $(CBFTOP)/cbf_alloc.c
SOURCES += $(CBFTOP)/cbf_ascii.c
SOURCES += $(CBFTOP)/cbf_binary.c
SOURCES += $(CBFTOP)/cbf_byte_offset.c
SOURCES += $(CBFTOP)/cbf_canonical.c
SOURCES += $(CBFTOP)/cbf_codes.c
SOURCES += $(CBFTOP)/cbf_compress.c
SOURCES += $(CBFTOP)/cbf_context.c
SOURCES += $(CBFTOP)/cbf_file.c
SOURCES += $(CBFTOP)/cbf_getopt.c
SOURCES += $(CBFTOP)/cbf_lex.c
SOURCES += $(CBFTOP)/cbf_packed.c
SOURCES += $(CBFTOP)/cbf_predictor.c
SOURCES += $(CBFTOP)/cbf_read_binary.c
SOURCES += $(CBFTOP)/cbf_read_mime.c
SOURCES += $(CBFTOP)/cbf_simple.c
SOURCES += $(CBFTOP)/cbf_string.c
SOURCES += $(CBFTOP)/cbf_stx.c
SOURCES += $(CBFTOP)/cbf_tree.c
SOURCES += $(CBFTOP)/cbf_uncompressed.c
SOURCES += $(CBFTOP)/cbf_write.c
SOURCES += $(CBFTOP)/cbf_write_binary.c
SOURCES += $(CBFTOP)/cbf_ws.c
SOURCES += $(CBFTOP)/md5c.c
